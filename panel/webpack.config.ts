import config from '../core/config'
import * as path from 'path'
import * as VueLoaderPlugin  from 'vue-loader/lib/plugin'
import * as CopyWebpackPlugin  from 'copy-webpack-plugin'
import { DefinePlugin, ContextReplacementPlugin } from 'webpack'

module.exports = {
  mode: 'development',
  devtool: 'source-map',
  entry: path.join(__dirname, 'frontend.ts'),
  output: {
    path: path.join(config.buildPath, 'panel'),
    filename: 'panel.js'
  },
  
  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
        options: {
          loaders: {
            ts: 'ts-loader'
          },
        }
      },
      {
        test: /\.ts$/,
        loader: 'ts-loader',
        exclude: /node_modules|.*\.vue/,
        // options: {
        //   appendTsSuffixTo: [/\.vue$/],
        // }
      },
      // this will apply to both plain `.js` files
      // AND `<script>` blocks in `.vue` files
      {
        test: /\.js$/,
        loader: 'babel-loader'
      },
      // this will apply to both plain `.css` files
      // AND `<style>` blocks in `.vue` files
      {
        test: /\.css$/,
        use: [
          'vue-style-loader',
          'css-loader'
        ]
      }
    ]
  },
  resolve: {
    extensions: [".ts", ".vue", ".js"],
    alias: {
      vue: 'vue/dist/vue.esm.js'
    }
  },
  plugins: [
    new DefinePlugin({
      "__API_URL__": JSON.stringify(config.apiUrl)
    }),
    // make sure to include the plugin for the magic
    new VueLoaderPlugin(),
    new CopyWebpackPlugin([
      {
        from: path.join(__dirname, 'assets', '*'),
        to: '.',
        toType: 'dir',
        flatten: true
      }
    ]),
    new ContextReplacementPlugin(/moment[\/\\]locale$/, /de/) // exclude all but german locales
  ]
}