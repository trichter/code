import { Context, Request } from "koa";
import DB from '../core/lib/DB'

declare module "koa" {
    /**
     * See https://www.typescriptlang.org/docs/handbook/declaration-merging.html for
     * more on declaration merging
     */
    interface Context {
        db: DB,
    }
}
declare var __API_URL__: string