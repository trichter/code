import { Event, Group } from '../../core/lib/types'
declare var __API_URL__
const APIUrl = __API_URL__

async function get(path): Promise<any> {
    try {
        const res = await fetch(`${APIUrl}${path}`)
        if(res.status != 200) throw new Error(await res.text())
        return await res.json()
    } catch(err) {
        alert('Fehler: '+err.message)
        throw err
    }
}
async function post(path, data={}): Promise<any> {
    try {
        const res = await fetch(`${APIUrl}${path}`, {
            method: 'POST',
            body: JSON.stringify(data),
            headers:{
                'Content-Type': 'application/json'
            }
        })
        if(res.status != 200) throw new Error(await res.text())
        return await res.json()
    } catch(err) {
        alert('Fehler: '+err.message)
        throw err
    }
}

export async function getStatus(): Promise<any> {
    return await get(`/status`)
}

export async function getGroups(branch: string): Promise<Group[]> {
    return await get(`/${branch}/groups`)
}

export async function fork(): Promise<{newBranch: string}> {
    return await post(`/newbranch`)
}
export async function upsertGroup(branch: string, group: Group, commitMessage: string) {
    return await post(`/${branch}/groups`, {
        group: group,
        commitMessage: commitMessage
    })
}

export async function getLog(branch: string): Promise<any> {
    return await get(`/${branch}/log`)
}
export async function getGroupEvents(branch: string, groupKey: string): Promise<Event[]> {
    return await get(`/${branch}/group/${groupKey}/events`)
}

export async function eventTransform2Manual(branch: string, groupKey: string, eventId: string): Promise<Event[]> {
    return await post(`/${branch}/group/${groupKey}/event/${eventId}/transform2manual`)
}

export async function upsertEvent(branch: string, groupKey: string, event: Event, commitMessage: string) {
    return await post(`/${branch}/group/${groupKey}/events`, {
        event: event,
        commitMessage: commitMessage
    })
}
export async function createMergeRequest(branch: string, message: string, author: string, email: string): Promise<{link: string}> {
    return await post(`/${branch}/mergerequest`, {
        message,
        author,
        email
    })
}