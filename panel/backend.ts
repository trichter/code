
import * as Koa from 'koa';
import * as bodyParser from 'koa-bodyparser';
import * as cors from '@koa/cors';
import router from './routes/index'
import config from '../core/config'
import DB from '../core/lib/DB'

export function startPanelBackend(db: DB) {
    const app = new Koa();
    // Enable cors with default options
    app.use(cors());

    // Enable bodyParser with default options
    app.use(bodyParser());

    app.use(async (ctx, next) => {
        console.log(`${ctx.method} ${ctx.path}`)
        ctx.db = db
        try {
            await next()
        } catch(err) {
            ctx.status = 500
            ctx.body = err.message
            console.error(err)
        }
    })
    app.use(router.routes()).use(router.allowedMethods());


    app.listen(config.apiPort);

    console.log(`panel backend is running on port ${config.apiPort}`);

}

if (require.main === module) { 
    const db = new DB(config.repoPath)
    startPanelBackend(db)
}