import * as Router from 'koa-router';
import * as group from './group';
import * as events from './events';
import * as branch from './branch';


const router = new Router();

router.get(`/status`, (ctx) => {
    ctx.body = {
        status: 'running'
    }
})
router.post(`/newbranch`, branch.createBranch)
router.post('/:branch/mergerequest', branch.createMergeRequest);
router.get('/:branch/log', branch.log);
router.get('/:branch/groups', group.getGroupList);
router.get('/:branch/group/:groupKey/events', events.getGroupEvents);
router.post('/:branch/group/:groupKey/events', events.upsertEvent);
router.post('/:branch/groups', group.upsertGroup);
router.post('/:branch/group/:groupKey/event/:eventId/transform2manual', events.transform2manual);

export default router
