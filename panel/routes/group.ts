import { Context } from 'koa';
import Crawler from '../../core/lib/Crawler'
export async function getGroupList(ctx: Context) {
    const groups = await ctx.db.getGroupList(ctx.params.branch)
    ctx.status = 200
    ctx.body = groups
}

export async function upsertGroup(ctx: Context) {
    const body = (<any>ctx.request).body
    let branch = ctx.params.branch
    let group = body.group
    let commitMessage = body.commitMessage
    if(!group || typeof group !== 'object' || !group.key) throw new Error('no group data provided')
    if(!commitMessage || typeof commitMessage !== 'string' || !commitMessage.trim()) throw new Error('no commit message provided')
    if(!branch || !branch.match(/^changes-[0-9]+$/)) throw new Error('branch is not writeable')
    
    if(!group.autocrawl || !group.autocrawl.source) group.autocrawl = null 
    if(await ctx.db.upsertGroup(branch, group)) {
        await ctx.db.commit(branch, commitMessage)
    }
    try {
        await Crawler.crawlGroup(ctx.db, branch, group, true)
    } catch(err) {
        err.message = `Error while auto-crawling group: ${err.message}`
        throw err
    }
    ctx.status = 200
    ctx.body = {}
}

