import { Context } from 'koa';
import { Group } from '../../core/lib/types'
import { decodeEventFrontmatter } from '../../core/lib/utils'
 
function isInArray(arr: Array<any>, el: any): boolean {
    let arr2 = arr.map(a => a.toString())
    return arr.indexOf(el.toString()) !== -1
}

export async function getGroupEvents(ctx: Context) {
    console.log(ctx.params)
    ctx.body = await ctx.db.getEvents(ctx.params.branch, <Group>{key: ctx.params.groupKey})
    ctx.status = 200
}
export async function transform2manual(ctx: Context) {
    const branch = ctx.params.branch
    if(!branch || !branch.match(/^changes-[0-9]+$/)) throw new Error('branch is not writeable')
    const group = await ctx.db.getGroup(branch, ctx.params.groupKey)
    if(!group) return ctx.status = 404
    const groupEvents = await ctx.db.getEvents(branch, group)
    const event = groupEvents.find(event => event.id == ctx.params.eventId)
    if(!event) return ctx.status = 404

    event.isCrawled = false
    if(group.autocrawl) {
        
        if(group.autocrawl.exclude) {
            if(!isInArray(group.autocrawl.exclude, event.id)) group.autocrawl.exclude.push(event.id)
        }
        else group.autocrawl.exclude = [event.id]
    }

    await ctx.db.upsertGroup(branch, group)
    await ctx.db.upsertEvent(branch, event, groupEvents)
    await ctx.db.commit(branch, `[${group.key}] '${event.title}' wird nun nicht mehr automatisch gecrawled`)
    ctx.body = {
        success:true
    }
}



export async function upsertEvent(ctx: Context) {
    const body = (<any>ctx.request).body
    let branch = ctx.params.branch
    if(!branch || !branch.match(/^changes-[0-9]+$/)) throw new Error('branch is not writeable')
    let group = await ctx.db.getGroup(branch, ctx.params.groupKey)
    if(!group) throw new Error('group not found')

    let commitMessage = body.commitMessage
    if(!commitMessage || typeof commitMessage !== 'string' || !commitMessage.trim()) throw new Error('no commit message provided')
    let event = decodeEventFrontmatter(body.event)
    event.group = group
    // event.start = 
    if(await ctx.db.upsertEvent(branch, event)) {
        await ctx.db.commit(branch, commitMessage)
    }
    ctx.status = 200
    ctx.body = {}
}