import { Context } from 'koa';
import { MergeRequests } from 'gitlab'
import config from '../../core/config'


const gitlabMergeRequests = new MergeRequests({
    url:   'https://gitlab.com',
    token: config.gitlabToken
})



   

export async function createBranch(ctx: Context) {
    const branches = await ctx.db.getBranchList()
    const lastBranch = branches
        .filter(name => name.match(/^changes-[0-9]+$/))
        .sort()
        .reverse()[0]
    const lastBranchIndex = lastBranch ? parseInt(lastBranch.slice(8)) : 0
    const newBranch = `changes-${lastBranchIndex+1}`

    await ctx.db.fork(newBranch)
    ctx.status = 200
    ctx.body = {
        newBranch: newBranch
    } 
}

export async function log(ctx: Context) {
    const log = await ctx.db.getLog(ctx.params.branch)
    ctx.status = 200
    ctx.body = log
}

export async function createMergeRequest(ctx: Context) {
    const branch = ctx.params.branch
    const body = (<any>ctx.request).body
    if(!branch || !branch.match(/^changes-[0-9]+$/)) throw new Error('branch is not writeable')
    if(!body.message.trim()) throw new Error('no message provided')
    if(!body.author.trim()) throw new Error('no author provided')
    if(!body.email.trim()) throw new Error('no email provided')

    const changedFiles = await ctx.db.getChangedFileList(branch)
    const changdGroups = changedFiles.reduce((o,file) => {
        let [group, event] = file.split('/')
        if(event) {
            if(o.indexOf(group) == -1) o.push(group)
        } 
        return o
    }, [])
    
    await ctx.db.push(branch)
    const res = await gitlabMergeRequests.create(config.gitlabProjectId, branch, 'master', `[panel] ${body.message}`, {
        description: `- Autor*in: ${body.author}\n- Kontakt: ${body.email}\n\n*Dieser Merge-Request wurde mit dem trichter Panel erstellt*`,
        remove_source_branch: true,
        allow_collaboration: true,
        labels: changdGroups ? changdGroups.join(',') : null
    })
    ctx.status = 200
    ctx.body = {
        status: 'success',
        link: res.web_url
    }
}
