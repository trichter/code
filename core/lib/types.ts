import * as moment from 'moment'


export interface recurringRule {
    units: number|number[],
    measure: 'days'|'weeks'|'months'|'years'|'dayOfWeek'|'dayOfMonth'|'weekOfMonth'|'weekOfYear'|'monthOfYear'
}

export interface recurringOptions {
    rules: recurringRule[]
    end?: moment.Moment,
    exceptions?: moment.Moment[]
}

export interface Group {
    key: string
    name: string
    website: string
    email?: string
    address?: string
    autocrawl?: {
        source: string,
        options?: any,
        exclude: string[]
        filter: {[s:string]: string}
    }
    description?: string
    // path: string
} 

export interface Event {
    _filename?: string
    group?: Group
    id: string
    parentId?: string
    title: string
    start: moment.Moment
    end: moment.Moment
    locationName?: string
    address: string
    link?: string
    image?: string
    teaser?: string
    description?: string
    recurring?: recurringOptions
    isCrawled?: boolean
}

export interface EventMultiGroup extends Event {
    groups: Group[]
}

export interface Source {
    crawl(options?: any): Promise<Event[]>
}



export interface IRecurringRule {
    units: number|number[],
    measure: 'days'|'weeks'|'months'|'years'|'dayOfWeek'|'dayOfMonth'|'weekOfMonth'|'weekOfYear'|'monthOfYear'
}

export interface IRecurringOptions {
    rules: IRecurringRule[]
    end?: string,
    exceptions?: string[]
}

export interface IEventFrontmatter {
    id: string
    title: string
    start: string
    end: string
    locationName?: string
    address: string
    link: string
    image?: string
    teaser?: string
    description?: string
    recurring?: IRecurringOptions
    isCrawled?: boolean
}