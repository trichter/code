import { Group, Event, EventMultiGroup, IEventFrontmatter } from './types'
import * as fm from 'front-matter'
import * as moment from 'moment'
import * as yaml from 'js-yaml'
import 'moment-recur-ts'



interface IGroupFrontmatter {
    name: string
    website: string
    email?: string
    address?: string
    autocrawl?: {
        source: string,
        options?: any,
        exclude?: Array<string|number>,
        filter?: {[s:string]: string}
    }
    withoutLabel?: boolean
}


export function validateEvent(event: Event) {
    if(!event.id || (typeof event.id !== 'string' && typeof event.id !== 'number')) throw new Error('id is undefined or invalid')
    if(!event.title || typeof event.title !== 'string' || !event.title.trim()) throw new Error('title is undefined or invalid')
    if(!event.start || !(event.start instanceof moment) || !event.start.isValid()) throw new Error('start time is undefined or invalid')
    // if(!event.address || typeof event.address !== 'string' || !event.address.trim()) throw new Error('address is undefined or invalid')
    if(event.recurring) {
        if(typeof event.recurring !== 'object') throw new Error('recurring is not of type object')
        if(!event.recurring.rules || !event.recurring.rules.length) throw new Error('recurring: you need to specify at least one rule')
        for(let rule of event.recurring.rules) {
            if(
                typeof rule !== 'object'
                 || !rule.units
                 || (typeof rule.units !== 'number' && typeof rule.units !== 'string')
                 || !rule.measure
                 || typeof rule.measure !== 'string'
            ) throw new Error('recurring: rule got an incorrect format')
        }

    }
}

export function validateGroup(group: Group) {
    if(!group.key || typeof group.key !== 'string') throw new Error('key is undefined or invalid')
    if(!group.name || typeof group.name !== 'string' || !group.name.trim()) throw new Error('group name is empty, undefined or not a ')
    if(!group.website || typeof group.website !== 'string' || !group.website.match(/^(http|https):\/\/.{3,}/)) throw new Error('website is undefined or invalid')

    if(group.autocrawl) {
        if(typeof group.autocrawl !== 'object') throw new Error('invalid autocrawl property')
        if(typeof group.autocrawl.source != 'string' || !group.autocrawl.source.trim()) throw new Error('for autocrawling, a source must be defined')
        if(group.autocrawl.exclude &&  !Array.isArray(group.autocrawl.exclude)) throw new Error('autocrawling exclude must be an array')

        group.autocrawl = {
            source: group.autocrawl.source,
            exclude: group.autocrawl.exclude ? group.autocrawl.exclude.map(e => e.toString()) : [],
            filter: group.autocrawl.filter || null, 
            options: group.autocrawl.options || null 
        }
        if(group.autocrawl.options) group.autocrawl.options = group.autocrawl.options
    }
}

export function encodeEventFile(event: Event): string {
    validateEvent(event)

    const obj: IEventFrontmatter = {
        id: event.id,
        title: event.title,
        start: event.start.format('YYYY-MM-DD HH:mm'),
        end: event.end ? event.end.format('YYYY-MM-DD HH:mm') : null,
        locationName: event.locationName || null,
        address: event.address || null,
        link: event.link || null,
        image: event.image || null,
        teaser: event.teaser || null,
        recurring: event.recurring && event.recurring.rules ? {
            rules: event.recurring.rules,
            end: event.recurring.end ? event.recurring.end.format(`YYYY-MM-DD`) : null,
            exceptions: event.recurring.exceptions ? event.recurring.exceptions.map(e => e.format(`YYYY-MM-DD`)) : []
        } : null,
        isCrawled: !!event.isCrawled
    }
    const fileContent = [
        '---',
        yaml.safeDump(obj, {
            lineWidth: 1000
        }).trim(),
        '---',
        (event.description || '')
    ].join('\n')
    return fileContent
}

export function decodeEventFile(body: string, group: Group): Event {
    const f: {attributes:IEventFrontmatter, body: string} = fm(body) 
    const event = decodeEventFrontmatter(f.attributes)
    event.group = group
    event.description = f.body.trim()
    return event
}
export function decodeEventFrontmatter(fm: IEventFrontmatter): Event {
    let event: Event = {
        id: fm.id,
        title: fm.title,
        image: fm.image,
        start: moment(fm.start),
        end: fm.end ? moment(fm.end) : null,
        locationName: fm.locationName,
        address: fm.address,
        link: fm.link,
        teaser: fm.teaser,
        recurring: fm.recurring ? {
            rules: fm.recurring.rules,
            end: fm.recurring.end ? moment(fm.recurring.end) : null,
            exceptions: fm.recurring.exceptions ? fm.recurring.exceptions.map(e => moment(e)) : [] 
        } : null,
        isCrawled: !!fm.isCrawled
    }
    if(event.recurring) {
        let rec = event.recurring
        if(rec.exceptions) rec.exceptions = rec.exceptions.map(e => moment(e))
    }
    validateEvent(event)
    return event
}

export function encodeGroupFile(group: Group): string {
    validateGroup(group)

    const obj: IGroupFrontmatter = {
        name: group.name,
        website: group.website,
        address: group.address || null,
        email: group.email || null,
        autocrawl: group.autocrawl ? {
            source: group.autocrawl.source,
            options: group.autocrawl.options || null,
            exclude: group.autocrawl.exclude || [],
            filter: group.autocrawl.filter || null,
        } : null
    }
    const fileContent = [
        '---',
        yaml.safeDump(obj, {
            lineWidth: 1000
        }).trim(),
        '---',
        (group.description || '')
    ].join('\n')
    return fileContent
}

export function decodeGroupFile(key: string, data: string): Group {
    let group: Group = {
        key: key,
        name: null,
        website: null,
        email: null,
        address: null,
        autocrawl: null,
        description: null,
    }
    const frontmatter = fm(data);
    const head = <IGroupFrontmatter>frontmatter.attributes


    if(!head.withoutLabel) {
        if(typeof head.name != 'string' || !head.name.trim()) throw new Error('group name is empty, undefined or not a string')
        if(typeof head.website != 'string' || !head.website.trim()) throw new Error('group website link is empty, undefined or not a string')

        group.name = head.name
        group.website = head.website
        group.email = head.email
        group.address = head.address
    }
    if(head.autocrawl) {
        if(typeof head.autocrawl !== 'object') throw new Error('invalid autocrawl property')
        if(typeof head.autocrawl.source != 'string' || !head.autocrawl.source.trim()) throw new Error('for autocrawling, a source must be defined')
        if(head.autocrawl.exclude &&  !Array.isArray(head.autocrawl.exclude)) throw new Error('autocrawling exclude must be an array')
        // TODO: checks of autocrawl.filter

        group.autocrawl = {
            source: head.autocrawl.source,
            exclude: head.autocrawl.exclude ? head.autocrawl.exclude.map(e => e.toString()) : [],
            filter: head.autocrawl.filter || {} 
        }
        if(head.autocrawl.options) group.autocrawl.options = head.autocrawl.options
    }
    group.description = frontmatter.body.trim()
    validateGroup(group)
    return group
}


export function expandRecurringEvents(event: Event): Event[] {
    if(!event.recurring) throw new Error('event is not recurring')
    let recur = moment(event.start).recur(Object.assign({start: event.start, end: moment(event.start).add('1', 'year')}, event.recurring))
    let dates = recur.next(10)
    let diff = event.end ? event.end.diff(event.start, 'minutes') : null
    dates.unshift(moment(event.start))

    return dates.map(date => {
        let clone = Object.assign({}, event)
        // let clone: Event = Object.assign(Object.create(Object.getPrototypeOf(event)), event); 
        clone.start = date.hours(event.start.utc().hours()).local(true)
        if(this.end) clone.end = moment(date).add(diff, 'minutes').local(true)
        return clone
    })

}

export function groupEventsByDays(events: Event[]): {[date: string]: Event[]} {
    let dates = {}
    
    for(let event of events) {
        let date = moment(event.start).utc().format('YYYY-MM-DD')
        if(!dates[date]) dates[date] = []
        dates[date].push(event)
    }

    // sort by time
    for(let date in dates) dates[date] = dates[date]
        .sort( (a,b) => a.end.unix()-b.end.unix())
        .sort( (a,b) => a.start.unix()-b.start.unix())
    
    return dates
}

export function groupDuplicatedEvents(dayEvents: {[date: string]: Event[]}): {[date: string]: EventMultiGroup[]} {
    let res = {}
    for(let day in dayEvents) {
        res[day] = []
        for(let event of dayEvents[day]) {
            let lastEvent: EventMultiGroup = res[day][res[day].length-1]
            if(lastEvent && lastEvent.title == event.title && lastEvent.link == event.link && lastEvent.address == event.address) {
                lastEvent.groups.push(event.group)
            } else {
                res[day].push(Object.assign({
                    groups: [event.group]
                }, event))
            }
        }
    }
    return res
}