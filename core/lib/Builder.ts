import { promises as fs } from 'fs'
import * as path from 'path'
import * as Twig from 'twig';
import * as moment from 'moment'

import { Event, Group } from './types'
import DB from './DB'
import config from '../config'
import { groupEventsByDays, groupDuplicatedEvents } from './utils'
// ==============================
// data

moment.locale('de')
// custom filters
Twig.extendFilter('isToday', (value) => moment(value).isSame(moment(), 'day'))
Twig.extendFilter('isTomorrow', (value) => moment(value).isSame(moment().add(1, 'day'), 'day'))
Twig.extendFilter('dayName', (value) => moment(value).format('dddd'))
Twig.extendFilter('time', (value) => moment(value).format('HH:mm'))
Twig.extendFilter('tidyUpAddress', (address) => {
    if(!address || !address.trim()) return ''
    return address
        .split(', ')
        .map(a => a.replace(/[0-9]{5}( |$)/, ''))
        .map(a => a.replace(/straße/, 'str.').trim())
        .filter(a => a.trim() && !a.match(/^(Leipzig|Germany|Deutschland|Sachsen|Saxony-Anhalt)$/))
        .join(', ')
})

const template = Twig.twig({
    id:   'fs-node-sync',
    path: path.join(__dirname, '../templates/list.twig'),
    async: false
})



export default class Builder {
    static async getEvents(db: DB, branch: string): Promise<Event[]> {
        return (await db.getAllEvents(branch, true))
            // filter out past events
            .filter(e => e.end ? e.end.isAfter(moment()) : e.start.add(2, 'hours').isAfter(moment()))
            .filter(e => moment(e.start).add(3, 'days').isAfter(moment()))
    }
    static async build(db: DB, branch: string) {
        const events = await this.getEvents(db, branch)
        const eventsByDays = groupDuplicatedEvents(groupEventsByDays(events))
        const days = Object.keys(eventsByDays)
        // TODO: bundle events with same link & title 

        // ==============================
        // render the template
        const rendered = template.render({
            title: 'trichter.cc | Veranstaltungen in Leipzig',
            days: Object.keys(eventsByDays).sort().slice(0,10),
            eventsByDays:  eventsByDays,
        })
        .replace(/\s{2,}/g, ' ') // simple minifier

        await fs.writeFile(path.join(config.buildPath, 'index.html'), rendered)

    }
}
