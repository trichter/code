import * as moment from 'moment'
import Facebook from '../sources/facebook'
import iCal from '../sources/ical'
import Frauenkultur from '../sources/frauenkultur'
import Rosalux from '../sources/rosalux'
import { Group, Source } from './types'
import DB from './DB'

export default class Crawler {
    static sources: {[key: string]: Source} = {
        facebook: new Facebook,
        ical: new iCal,
        frauenkultur: new Frauenkultur,
        rosalux: new Rosalux,
    }

    static async crawl(db: DB, branch: string, autoCommit?: boolean) {
        let groups = await db.getGroupList(branch);
        for(let group of groups) {
            if(!group.autocrawl) continue
            try {
                await this.crawlGroup(db, branch, group, autoCommit)
            } catch(err) {
                console.error(err)
            }
        }
    }
    static async crawlGroup(db: DB, branch: string, group: Group, autoCommit?: boolean) {
        if(!group.autocrawl) return
        console.log(`# crawling ${group.key}`)
        let existingGroupEvents = await db.getEvents(branch, group);
        let source = this.sources[group.autocrawl.source]

        if(!source) throw new Error(`source '${group.autocrawl.source}' does not exsist`)
        let events = await source.crawl(group.autocrawl.options)
        const exclude = group.autocrawl.exclude && group.autocrawl.exclude.length ? group.autocrawl.exclude.map(id => id.toString()) : [] // we use strings for .indexOf()

        // TODO: handle excluded events in diff
        let diff: number|string = events.length-existingGroupEvents.filter(e => e.start.isAfter(moment())).length
        if(diff > 0) diff = '+'+diff
        console.log(`got ${events.length} events ${diff != 0 ? `(${diff})` : ''}`)

        for(let event of events) {
            if(!event) continue
            if(exclude.indexOf(event.id.toString()) != -1) continue
            if(event.parentId && exclude.indexOf(event.parentId.toString()) != -1) continue

            // exclude events, which costs money
            if(event.description && event.description.match(/[0-9]\s*€/) && !event.description.match(/Spendenempfehlung/i)) continue
            
            if(group.autocrawl.filter) {
                let filter = false
                for(let key in group.autocrawl.filter) {
                    let regex = new RegExp(group.autocrawl.filter[key], 'i')
                    if(!regex.test(event[key])) {
                        filter = true
                        break
                    }
                }
                if(filter) continue
            }
            event.group = group
            event.isCrawled = true
            console.log(`- ${event.start.format("YYYY-MM-DD")} ${event.title.substr(0,50)}`)
            await db.upsertEvent(branch, event, existingGroupEvents)
        }

        if(autoCommit) await db.commit(branch, `[${group.key}] autocrawled changes`, group.key)
    }
}