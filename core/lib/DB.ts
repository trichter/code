import { promises as fs } from 'fs'
import * as path from 'path'
import * as simplegit from 'simple-git/promise';
import { encodeEventFile, decodeEventFile, encodeGroupFile, decodeGroupFile, expandRecurringEvents } from './utils'
import { Group, Event } from './types'

type LogEntry = {hash:string, message: string, date: string}

function eventFileName(event: Event) {
    return `${event.start.format('YYYY-MM-DD')} ${event.title.replace(/([\/\?<>\\:\*\|":]|[\x00-\x1f\x80-\x9f]|^\.+$)/g, '').slice(0,40)}.md`
}

export default class DB {
    path: string
    git: simplegit.SimpleGit
    lock: boolean = false
    constructor(path: string) {
        this.path = path
        this.git = simplegit(path)
    }
    async getGroup(branch: string, key: string): Promise<Group> {
        let group
        await this.doLocked( async() => {
            await this.checkout(branch)
            group = await this._getGroup(key)
        })
        return group
    }
    private async _getGroup(key: string): Promise<Group> {
        if(key == '_') {
            // dummy group for events, which are not assigned to one
            return {
                key: '_',
                name: null,
                website: null
            }
        }
        try {
            let file = await fs.readFile(path.join(this.path, key, 'group.md'), 'utf8')
            return decodeGroupFile(key, file)
        } catch(err) {
            err.message = `Error while parsing '${key}/group.md': ${err.message}`
            throw err
        }
    }
    async getGroupList(branch: string): Promise<Group[]> {
        let groups: Group[] = []
        await this.doLocked( async () => {
            await this.checkout(branch)
            let folders = await fs.readdir(this.path)
            for(let folder of folders) {
                if(folder === 'README.md' || folder[0] === '.') continue
                if(!(await fs.stat(path.join(this.path, folder))).isDirectory()) continue
                groups.push(await this._getGroup(folder))
            }
        })
        return groups
    }

    async getEvents(branch: string, group: Group, expandRecurring: boolean = false): Promise<Event[]> {
        let events: Event[] = []

        await this.doLocked( async() => {
            await this.checkout(branch)
            let files = await fs.readdir(path.join(this.path, group.key))
            for(let filename of files) {
                if(filename.split('.')[0] === 'group' || filename[0] === '.') continue
                try {
                    let file = await fs.readFile(path.join(this.path, group.key, filename), 'utf8')
                    let event = decodeEventFile(file, group)
                    event._filename = filename
                    if(expandRecurring && event.recurring) {
                        for(let e of expandRecurringEvents(event)) events.push(e)
                    } else {
                        events.push(event)
                    }
                } catch(err) {
                    err.message = `Error while parsing Event '${group.key}/${filename}': ${err.message}`
                    throw err
                }
            }
        })

        return events
    }
    async upsertEvent(branch: string, event: Event, groupEvents: Event[] = null): Promise<boolean> {

        // groupEvents can be passed to avoid the retrieving of existing group events in every single upsertEvent() call
        if(groupEvents === null) groupEvents = await this.getEvents(branch, event.group)
        let existingEvent = groupEvents.find((e) => e.id === event.id)
        let data = encodeEventFile(event)
        
        await this.doLocked( async() => {
            await this.checkout(branch)
            if(existingEvent) {
                let existingFilename = existingEvent._filename || eventFileName(existingEvent)
                // has filename changed? remove old file!
                if(existingFilename !== eventFileName(event)) {
                    await fs.unlink(path.join(this.path, event.group.key, existingFilename))
                }
            }
            let file = path.join(this.path, event.group.key, eventFileName(event))
            await fs.writeFile(file, data, 'utf8')
        })

        return true // TODO: false, if not changed
    }

    async getAllEvents(branch: string, expandRecurring: boolean = false): Promise<Event[]> {
        const groups = await this.getGroupList(branch)
        let events: Event[] = []
        for(let group of groups) {
            for(let event of await this.getEvents(branch, group, expandRecurring)) {
                events.push(event)
            }
        }
        return events
    }

    private async checkout(branchName: string): Promise<void> {
        // if(branchName == this.currentBranch) return
        await this.git.checkout(branchName)
    }

    async fork(branchName: string, ref: string = 'master'): Promise<void> {
        await this.doLocked( async () => {
            await this.git.checkoutBranch(branchName, ref)
        })
    }
    async getForkPoint(branchName: string, ref: string = 'master'): Promise<string> {
        return (await this.git.raw([
            'merge-base',
            '--fork-point',
            ref,
            branchName
        ])).trim()
    }
    async getLog(branchName: string, ref: string = 'master'): Promise<Array<LogEntry>> {
        const forkPoint = await this.getForkPoint(branchName, ref)
        let response
        await this.doLocked( async() => {
            await this.checkout(branchName)
            const log =  await this.git.log({
                from: branchName,
                to: forkPoint,
                format: {
                    hash: '%H',
                    message: '%s',
                    date: '%ai'
                }
            })
            response = log.all
        })
        return response
    }
    
    async commit(branch: string, message: string, path: string = '.'): Promise<void> {
        await this.doLocked( async () => {
            await this.checkout(branch)
            await this.git.add(path)
            await this.git.commit(message)
        })
    } 
    async getBranchList(): Promise<string[]> {
        return (await this.git.branch([])).all        
    }
    async upsertGroup(branch: string, group: Group): Promise<boolean> {
        let response 
        await this.doLocked(async() => {
            await this.checkout(branch)
            
            try {
                await fs.mkdir(path.join(this.path, group.key))
            } catch(err) {}

            let file = path.join(this.path, group.key, 'group.md')
            let content = encodeGroupFile(group)
    
            try {
                let oldContent = await fs.readFile(file, 'utf8')
                if(content === oldContent) {
                    // group has not changed
                    response = false
                    return
                }
            } catch(err) {}

            await fs.writeFile(file, content, 'utf8')
            response = true
        })

        return response
    }
    async deleteEvent(event: Event): Promise<boolean> {
        return null
        
    }

    async push(branch: string): Promise<void> {
        let res = await this.git.push('origin', branch+':'+branch)
    }

    async pull(branch: string): Promise<void> {
        let res = await this.git.pull('origin', branch+':'+branch)
    }
    async getChangedFileList(branch: string): Promise<string[]> {
        const forkPoint = await this.getForkPoint(branch)
        const res = await this.git.diff([branch, forkPoint, '--name-only', '-z'])
        return res.split('\x00').filter(f => f.trim())
    }
    // guarentees, that branch won't change during the execution of the async function 
    private async doLocked(func: () => Promise<void>) {
        let startTime = Date.now()
        while(this.lock) {
            await new Promise(resolve => setTimeout(resolve, 100))
            if(Date.now()-startTime > 2*60*60*1000) {
                // already locked longer then 2 hours?
                // -> we run into an unknown bug and will 
                // unsafelyremove the lock.
                // --
                // this locking process gets removed anyway 
                // in favour of multiple git working directories
                // https://stackoverflow.com/a/30185564
                this.lock = false
                console.log('UNSAFELY REMOVED LOCK DUE TO TIMEOUT (there is an unknown bug)')
            }
        }
        this.lock = true
        try {
            await func()
            this.lock = false
            return
        } catch(err) {
            this.lock = false
            throw err
        }
    }

}