import { Event, Source } from '../lib/types'
import fetch from 'node-fetch'
import * as moment from 'moment';

export default class Facebook implements Source {
    async crawl(options: {page_id: number|string}): Promise<Event[]> {
        if(!options || !options.page_id) throw new Error(`option 'page_id' must be provided`)
        return await this.getEvents(options.page_id.toString())
    }
    private async getEvents(pageId: string): Promise<Event[]> {
        let res = await fetch(`http://localhost:58234/fbpage/${pageId}`)
        if(res.status !== 200) {
            console.log(`could not get events for page ${pageId} (${await res.text()})`)
            return []
        }
        const eventsJson = await res.json()

        let events = []
        for(let e of eventsJson) {
            // TODO: store image
            events.push({
                group: null,
                id: e.id,
                parentId: e.parentId,
                title: e.title,
                start: moment(e.start),
                end: e.end ? moment(e.end) : null,
                address: e.address,
                locationName: e.locationName,
                link: e.link,
                image: e.image,
                teaser: e.teaser,
                description: e.description
            })
        }
        return events
    }
}