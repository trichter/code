import { Event, Source } from '../lib/types'
import fetch from 'node-fetch'
import * as moment from 'moment';
import * as cheerio from 'cheerio'
import * as ical from 'ical'

const icalParseURL = (url, options) => new Promise( (resolve,reject) => ical.fromURL(url, options, (err,data) => {
    if(err) reject(err)
    else resolve(data)
}))

async function getIds(city: string, year: number, month: number) {
    let ids = []
    const res = await fetch(`https://www.rosalux.de/veranstaltungen/?tx_cbelasticsearch_searchform[year]=${year}&tx_cbelasticsearch_searchform[month]=${month}`)
    const body = await res.text()
    const $ = cheerio.load(body)

    $('.elasticsearch__result').each((index, el) => {
        let detail =  $('.teaser__date-group--right', el)
        if(detail.text().indexOf(city) == -1) return
        let link = $('a.teaser__liner', el)[0].attribs.href
        ids.push(link.match(/es_detail\/([A-Z0-9]+)\//)[1])
    })
    return ids
}

export default class Rosalux implements Source {
    async crawl(options: {city: string, eventUrl: string}): Promise<Event[]> {
        const now = moment()
        const nextMonth = moment().add(1, 'month')
        let ids = [].concat(
            await getIds(options.city, now.year(), now.month()+1),
            await getIds(options.city, nextMonth.year(), nextMonth.month()+1),
        )
        let events = []
        for(let id of ids) {
            const ical = await icalParseURL(`https://www.rosalux.de/veranstaltung/es_detail/ical/${id}/`, {})
            const e = ical[Object.keys(ical)[0]]
            if(!e.location) continue

            let event: Event = {
                id: id,
                link: options.eventUrl ? options.eventUrl.replace(/:id/, id) : `https://www.rosalux.de/veranstaltung/es_detail/${id}/`,
                title: e.summary,
                teaser: e.description || null,
                start: moment(e.start),
                end: e.end ? moment(e.end) : null,
                address: e.location
            }


            if(e.location.match(/,/g).length >= 2) {
                event.locationName = e.location.split(', ')[0]
                event.address = e.location.split(', ').slice(1).join(', ')
            }
            events.push(event)
        }

        return events
    }
}



