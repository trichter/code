import { Event, Source } from '../lib/types'
import fetch from 'node-fetch'
import * as moment from 'moment';
import * as cheerio from 'cheerio'

export default class Frauenkultur implements Source {
    async crawl(options: {url: string}): Promise<Event[]> {

        const res = await fetch(`https://www.frauenkultur-leipzig.de/Programm/Programm.html`)
        const body = await res.text()
        const $ = cheerio.load(body)
        console.log('done')
        const events = []
        $('.progammEntry')
            .each((index, el) => {
                const date = $(".programmDate", el).text()
                const time = $(".programmTime", el).text()
                const start = moment(date+', '+time, "DD.MM.YY, HH:mm");
                const title = $(".programmInfoHeader", el).text().trim()

                let event: Event = {
                    id: start.format("YYYYMMDD")+'-'+title.toLowerCase().replace(/\s+/g, ' ').replace(/[^a-z_]+/g, '').slice(0,16),
                    title: title,
                    start: start,
                    end: time.match(/ - /) ? moment(date+', '+time, "DD.MM.YY, ... HH:mm [Uhr]") : null,
                    address: `Windscheidstr. 51, 04277 Leipzig`,
                    locationName: `FraKu`,
                    description: $(".programmInfoText", el).text().trim().replace(/\t/g, '').replace(/[ ]{2,}/g, ' '),
                    link: `https://www.frauenkultur-leipzig.de/Programm/Programm.html#${start.format('D')}`
                }
                event.teaser = event.description.slice(0, 150)

                if(event.description.match(/Euro|ermäßigt|Teilnahmegebühr|VVK|Ticket/)) return
                if(event.start.isBefore(moment())) return
                events.push(event)
            })

        return events
    }
}



