import { Event, Source } from '../lib/types'
import * as ical from 'ical'
import * as moment from 'moment';

const icalParseURL = (url, options) => new Promise( (resolve,reject) => ical.fromURL(url, options, (err,data) => {
    if(err) reject(err)
    else resolve(data)
}))


export default class iCal implements Source {
    async crawl(options: {url: string}): Promise<Event[]> {
        const cal = await icalParseURL(options.url, {})

        return Object.keys(cal).map( (id) => {
            let c = cal[id]
            if(!c.summary) return null
            let event: Event = {
                id: id.split('@')[0],
                title: c.summary,
                start: moment(c.start),
                end: c.end ? moment(c.end) : null,
                address: c.location || null,
                link: typeof c.url == 'object' ? c.url.val : c.url,
                teaser: c.description ? c.description.substr(0,100) : null,
                description: c.description ? c.description.replace(/\n/gm, '\n\n') : null,
            }
            return event
        })
        .filter( (e) => {
            if(!e) return false
            if(e.start.isBefore(moment())) return false
            return true
        })
    }
}



