import * as yaml from 'js-yaml'
import * as path from 'path'
import * as fs from 'fs'

// defaults
let config = {
    repoPath: path.join(__dirname, '../repo'),
    buildPath: path.join(__dirname, '../build'),
    defaultBranch: 'master',
    apiPort: 51002,
    apiUrl: '', 
    gitlabToken: null,
    gitlabProjectId: null,
    crawlInterval: 30*60,
    rebuildInterval: 5*60
}
const configFile = process.env.TRICHTER_CONFIG_FILE || path.join(__dirname, '../trichterconfig.yaml')
try {
    let content = fs.readFileSync(configFile)
    try {
        let y = yaml.safeLoad(content)
        if(y.repoPath) config.repoPath = y.repoPath[0] == '/' ? y.repoPath : path.join(__dirname, '..', y.repoPath)
        if(y.buildPath) config.buildPath = y.buildPath[0] == '/' ? y.buildPath : path.join(__dirname,  '..', y.buildPath)
        if(y.defaultBranch) config.defaultBranch = y.defaultBranch
        if(y.apiPort) config.apiPort = y.apiPort
        config.apiUrl = `http://localhost:${config.apiPort}`
        if(y.apiUrl) config.apiUrl = y.apiUrl
        if(y.gitlabToken) config.gitlabToken = y.gitlabToken
        if(y.gitlabProjectId) config.gitlabProjectId = y.gitlabProjectId
        if(y.crawlInterval) config.crawlInterval = parseInt(y.crawlInterval)
        if(y.rebuildInterval) config.rebuildInterval = parseInt(y.rebuildInterval)

    } catch(err) {
        console.error(`unable to parse config file: ${err.message}`)
        process.exit(1)
    }
} catch(err) {
    // file does not exist, that's okay!
    if(process.env.TRICHTER_CONFIG_FILE) throw err
}

export default config
