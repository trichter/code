import DB from './lib/DB'
import Crawler from './lib/Crawler'
import Builder from './lib/Builder'
import config from './config'

let db = new DB(config.repoPath)

async function run() {
    switch(process.argv[2]) {
        case 'crawl':
            try {
                if(process.argv[3]) {
                    let group = await db.getGroup(config.defaultBranch, process.argv[3])
                    if(!group) throw new Error(`group '${process.argv[3]}' could not be found`)
                    await Crawler.crawlGroup(db, config.defaultBranch, group)
                } else {
                    await Crawler.crawl(db, config.defaultBranch)
                }
            } catch(err) {
                console.error(err)
                process.exit(1)
            }
            break
        case 'build':
            Builder.build(db, config.defaultBranch)
            break

        case 'build-scss':
            
        default:
            console.error(`usage: yarn cli crawl|build`)
    }
}
run()