import DB from './core/lib/DB'
import config from './core/config'
import Crawler from './core/lib/Crawler'
import Builder from './core/lib/Builder'

import { startPanelBackend } from './panel/backend'


async function build() {
    console.log(`[${(new Date).toISOString()}] building`)
    try {
        try {
            await db.pull(config.defaultBranch)
        } catch(err) {
            console.error(`[!] error while building:`)
            console.error(err)
        }
        await Builder.build(db, config.defaultBranch)
        console.log(`[${(new Date).toISOString()}] building done`)
    } catch(err) {
        console.error(`[!] error while building:`)
        console.error(err)
    }

}
async function crawl() {
    console.log(`[${(new Date).toISOString()}] crawling`)
    try {
        try {
            await db.pull(config.defaultBranch)
        } catch(err) {
            if(err.message.match(/kein Vorspulen/)) {
                await db.push(config.defaultBranch)
            } else {
                throw err
            }
        }
        await Crawler.crawl(db, config.defaultBranch, true)
        await db.push(config.defaultBranch)
        console.log(`[${(new Date).toISOString()}] crawling done`)
    } catch(err) {
        console.error(`[!] error while crawling:`)
        console.error(err)
    }

}
const db = new DB(config.repoPath)
startPanelBackend(db)

setInterval(build, config.rebuildInterval*1000)
setInterval(crawl, config.crawlInterval*1000)


;(async () => {
    await crawl()
    await build()
})()
